@extends('application')
@section('page-title')
    List
@endsection
@section('page-content')
    <div class="container">
        @if($search)
            <p class="mt-3">Search result for: {{ $search }}</p>
            <a href="{{route("promotion.index")}}" class="mb-5">Return to list</a>
        @endif
    </div>
    <div class="container">

        <a class="btn btn-outline-success mt-3" href="{{ route("promotion.create") }}">Add Promotion</a>

        <table class="table table-bordered mt-3">
            <thead>
            <tr>
                <th scope="col">Promo Name</th>
                <th scope="col">Speciality</th>
                <th scope="col">Students List</th>
                <th scope="col">Modules List</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($promotions as $promo)
                <tr>
                    <td>{{ $promo->name }}</td>
                    <td>{{ $promo->speciality }}</td>
                    <td>
                        @foreach($promo->students as $stud)
					        <li class="list-group" aria-current="true">{{ $stud->lastname." ".$stud->firstname }}</li>
                        @endforeach
                    </td>
                    <td>
                        @foreach($promo->modules as $modu)
					        <li class="list-group" aria-current="true">{{ $modu->name }}</li>
                        @endforeach
                    </td>
                    
                    <td class="d-flex" style="size: inherit">
                        <a class="btn btn-outline-success mr-2" href="{{ route("promotion.show", $promo) }}">Show</a>
                        <a class="btn btn-outline-info mr-2" href="{{ route("promotion.edit", $promo) }}">Edit</a>
                        <form action="{{ route("promotion.destroy", $promo->id) }}" method="post">
                            <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
