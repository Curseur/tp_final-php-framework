@extends("promotion.form")

@section('page-title')
    Edit Promotion
@endsection

@section('action'){{ route('promotion.update', $promo) }}@endsection

@section("method")@method("PUT")
@endsection

@section("name"){{ $promo->name }}
@endsection

@section("speciality"){{ $promo->speciality }}
@endsection
