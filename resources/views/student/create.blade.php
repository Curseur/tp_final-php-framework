@extends("student.form")
@section("page-title")Add Student @endsection
@section("action")
	{{ route("student.store") }}
@endsection
@section('promotion'){{ $promotion->name." ".$promotion->speciality }}@endsection
@section('promotion_id'){{ $promotion->id }}@endsection