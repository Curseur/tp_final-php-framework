@extends("student.form")
@section("page-title")
    Add Student
@endsection
@section("action"){{ route("student.update", $student) }}@endsection
@section("method") @method("PUT") @endsection
@section("lastname"){{ $student->lastname }}@endsection
@section("firstname"){{ $student->firstname }}@endsection
@section("email"){{ $student->email }}@endsection
@section("promotion")
    <select name="promotion" class="form-control">
        @foreach ($promotion as $promo)
            <option value="{{ $promo->id }}">{{ $promo->name." ".$promo->speciality }}</option>
        @endforeach
    </select>
@endsection