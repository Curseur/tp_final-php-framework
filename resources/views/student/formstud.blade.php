@extends('application')
@section('page-title')
    @yield('page-title')
@endsection
@section('page-content')
    <div class="container mb-5 mt-3">
        <form method="post" action="@yield('action')">
            @csrf
            <div class="form-row">
                <div class="col">
                    <label for="lastname">Lastname</label>
                    <input type="text" class="form-control" name="lastname" id="lastname" value="@yield("lastname")">
                </div>
                <div class="col">
                    <label for="firstname">Firstname</label>
                    <input type="text" class="form-control" name="firstname" id="firstname" value="@yield("firstname")">
                </div>
                <div class="col">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" value="@yield("email")">
                </div>
            </div>
            <label for="promotion">Promotion</label>
            <fieldset>
                <div class="form-row mb-5">
                  <div class="col">
                    @yield("promotion")
                  </div>
                </div>
            </fieldset>
            <input type="hidden" name="promotion_id" value="@yield("promotion_id")">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection