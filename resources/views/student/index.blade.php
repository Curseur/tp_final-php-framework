@extends('application')
@section('page-title')
    List
@endsection
@section('page-content')
    <div class="container">

        @if($search)
            <p class="mt-3">Search result for: {{ $search }}</p>
            <a href="{{route("student.index")}}" class="mb-5">Return to list</a>
        @endif

        <table class="table table-bordered mt-3">
            <thead>
            <tr>
                <th scope="col">Lastname</th>
                <th scope="col">Firstname</th>
                <th scope="col">Email</th>
                <th scope="col">Promotion</th>
                <th scope="col">Modules List</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($student as $stud)
                <tr>
                    <td>{{ $stud->lastname }}</td>
                    <td>{{ $stud->firstname }}</td>
                    <td>{{ $stud->email }}</td>
                    <td>{{ $stud->promotion->name." ".$stud->promotion->speciality }}</td>
                    <td>
                        @foreach($stud->modules as $modu)
					        <li class="list-group" aria-current="true">{{ $modu->name }}</li>
                        @endforeach
                    </td>
                    
                    <td class="d-flex" style="size: inherit">
                        <a class="btn btn-outline-success mr-2" href="{{ route("student.show", $stud) }}">Show</a>
                        <a class="btn btn-outline-info mr-2" href="{{ route("student.edit", $stud) }}">Edit</a>
                        <form action="{{ route("student.destroy", $stud->id) }}" method="post">
                            <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
