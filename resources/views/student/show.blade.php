@extends('application')
@section('page-title')
    {{ $student->lastname }} {{ $student->firstname }}
@endsection

@section('page-content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $student->lastname }} {{ $student->firstname }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{ $student->promotion->name." ".$student->promotion->speciality }}</h6>
                <p class="card-text">
                    <strong>Email: </strong>
                    {{ $student->email }}
                </p>
                <p class="card-text">
                    <strong>Modules: </strong>
                </p>
                <ul class="list-group list-group-flush mb-3">
                    @foreach($student->modules as $modu)
                        <li class="list-group-item">
                            <a href="{{ route("module.show", $modu) }}">{{ $modu->name }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="d-flex">
                    <a class="btn btn-outline-primary mr-2" href="{{ route("module.index", ["student"=>$student]) }}">Add Modules</a>
                    <a class="btn btn-outline-info mr-2" href="{{ route("student.edit", $student) }}">Edit</a>
                    <form action="{{ route("student.destroy", $student->id) }}" method="post">
                        <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                        @method('delete')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection