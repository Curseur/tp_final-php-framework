<a class="btn btn-outline-success mt-3" href="{{ route("module.create") }}">Create Module</a>

<table class="table table-bordered mt-3">
	<thead>
	<tr>
		<th scope="col">Module Name</th>
		<th scope="col">Description</th>
		<th scope="col">Promotions List</th>
		<th scope="col">Students List</th>
		<th scope="col">Actions</th>
	</tr>
	</thead>
	<tbody>

	@foreach($modules as $modu)
		<tr>
			<td>{{ $modu->name }}</td>
			<td>{{ $modu->description }}</td>
			<td>
				@foreach($modu->promotions as $promo)
					<li class="list-group" aria-current="true">{{ $promo->name." ".$promo->speciality }}</li>
				@endforeach
			</td>
			<td>
				@foreach($modu->students as $stud)
					<li class="list-group" aria-current="true">{{ $stud->lastname." ".$stud->firstname }}</li>
				@endforeach
			</td>

			<td class="d-flex" style="size: inherit">
				<a class="btn btn-outline-success mr-2" href="{{ route("module.show", $modu) }}">Show</a>
				<a class="btn btn-outline-info mr-2" href="{{ route("module.edit", $modu) }}">Edit</a>
				<form action="{{ route("module.destroy", $modu->id) }}" method="post">
					<input class="btn btn-outline-danger" type="submit" value="Delete"/>
					@method('delete')
					@csrf
				</form>
			</td>
		</tr>
	@endforeach

	</tbody>
</table>
