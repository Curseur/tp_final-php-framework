<form method="POST"
      action="{{route("student.store_modules", ["student_id" => $current_student_id])}}">
    @csrf
    @foreach($modules as $module)
        <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="module-{{ $module->id }}"
                   value="{{ $module->id }}" name="modules[]"
                    @foreach($module->students as $module_student)
                        @if($module_student->id == $current_student_id) checked @endif
                    @endforeach>
            <label class="form-check-label"
                   for="module-{{ $module->id }}">{{ $module->name }}</label>
        </div>
    @endforeach
    <button type="submit" class="btn btn-primary">Submit</button>
    <a type="submit" href="{{ route("module.create", ["student_id"=> $current_student_id]) }}"
       class="btn btn-primary">Create</a>
</form>
