@extends('application')
@section('page-title')Modules @endsection
@section('page-content')
  <div class="container">
    @if($search)
      <p class="mt-3">Search result for: {{ $search }}</p>
      <a href="{{route("module.index")}}" class="mb-5">Return to list</a>
    @endif
  </div>
  <div class="container">
    @if(isset($current_module_id))
      @include("module.parts.form_create")
    @else
      @include("module.parts.list")
    @endif
  </div>
@endsection
