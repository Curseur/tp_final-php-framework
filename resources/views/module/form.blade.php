@extends('application')
@section('page-title')
	@yield("title")
@endsection
@section('page-content')
	@if(!isset($current_promotion_id)) {{ $current_promotion_id = null }}  @endif
	<div class="container">
		<form method="POST" action="@yield("action")">
			@csrf
			@yield("method")
			<div class="mb-3">
				<label for="name" class="form-label">Module Name</label>
				<input type="text" class="form-control" id="name" name="name" value="@yield("name")">
			</div>
			<div class="mb-3">
				<label for="description" class="form-label">Description</label>
				<input type="text" class="form-control" id="description" name="description" value="@yield("description")">
			</div>
			<div class="mb-3">
			<label for="name" class="form-label">Promotions List</label>
			@yield("promotions-list")
			</div>
			<div class="mb-3">
			<label for="name" class="form-label">Students List</label>
			@yield("students-list")
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
@endsection
