<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PromotionController@index");
Route::resource("student", "StudentController");
Route::resource("promotion", "PromotionController");
Route::resource("module", "ModuleController");

Route::post("promotion/storemodules", "PromotionController@storeModules")
    ->name("promotion.store_modules");

Route::post("student/storemodules", "StudentController@storeModules")
    ->name("student.store_modules");