<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Module extends Model
{
    public function promotions(): BelongsToMany {
        return $this->belongsToMany(Promotion::class);
    }

    public function students(): BelongsToMany {
        return $this->belongsToMany(Student::class);
    }
}
