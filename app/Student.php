<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['lastname', 'firstname', 'email'];

    public function promotion() {
        return $this->belongsTo(Promotion::class);
    }

    public function modules() {
        return $this->belongsToMany(Module::class);
    }
}
