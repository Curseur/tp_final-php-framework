<?php

namespace App\Http\Controllers;

use App\Student;
use App\Promotion;
use App\Module;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return view("student.index", ["students" => Student::all(), "search" => null]);

        $student = Student::all();
        $search = $request->get("search");
        if ($search) {
            $student = Student::where('lastname', 'like', '%' . $search . '%')
            ->orWhere('firstname', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->get();
        // } elseif ($search) {
        //     $promotion = Promotion::where('name', 'like', '%' . $search . '%')
        //     ->orWhere('speciality', 'like', '%' . $search . '%')
        //     ->get();
        // }elseif ($search) {
        //     $module = Module::where('name', 'like', '%' . $search . '%')
        //     ->get();
        } else {
            $student = Student::all();
            // $promotion = Promotion::all();
            // $module = Module::all();
        }

        // return view("student.index", ["student" => $student, "promotions" => $promotion, "modules" => $module, "search" => $search]);
        return view("student.index", ["student" => $student, "search" => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Promotion $promotion)
    {
        $promotion_id = $request->input("promotion");

        // return view(
        //     "student.create",
        //     ["promotion" => Promotion::find($promotion_id)]
        // );

        $id = $request->input('id');
        return view (
            "student.createstud",
            [
                "id" => $id,
                "promotion" => Promotion::find($promotion_id)
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        // $newStudent = new Student();
        // $newStudent->lastname = $request->input("lastname");
        // $newStudent->firstname = $request->input("firstname");
        // $newStudent->email = $request->input("email");
        // $newStudent->promotion_id = $request->input("promotion_id");
        // $newStudent->save();

        // return redirect()->route("student.index");

        $student = new Student;
        $student->lastname = $request->input('lastname');
        $student->firstname = $request->input('firstname');
        $student->email = $request->input('email');
        $student->promotion_id = $request->input('promotion_id');
        $student->save();

        $promotion = Promotion::find($request->input('promotion_id'));
        return redirect()->route('promotion.show', ['promotion' => $promotion]);
    }

    public function storeModules(Request $request)
    {
        $student = Student::find($request->input("student_id"));
        $student->modules()->detach();
        $student->modules()->attach($request->input("modules"));

        return redirect()->route("module.index", ["student" => $student->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student): View
    {
        return view("student.show", ["student" => $student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student): View
    {
        $promotion = Promotion::All();
        return view("student.edit", ['student' => $student, 'promotion' => $promotion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $lastname = $request->input('lastname');
        $firstname = $request->input('firstname');
        $email = $request->input('email');
        $promotion_id = $request->input('promotion');

        $student = Student::find($student->id);
        $student->lastname = $lastname;
        $student->firstname = $firstname;
        $student->email = $email;
        $student->promotion_id = $promotion_id;
        $student->push();
        return redirect()->route('student.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('student.index');
    }
}
