<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promotions')->delete();
        factory(\App\Promotion::class, 20)->create()->each(function ($promotion) {
            $promotion->save();

            $faker = Faker\Factory::create();

            for ($i = 0; $i < 2; $i++) {
                DB::table('students')->insert(
                    [
                        "promotion_id" => $promotion->id,
                        "lastname" => $faker->lastName,
                        "firstname" => $faker->firstName,
                        "email" => $faker->email
                    ]
                );
            }
            
        });
    }
}
